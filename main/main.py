import time
# from libraries.MFRC522 import MFRC522, GPIO
# from util.constants import FridgeHardware, FridgeStatus
# from libraries.rfid_reader import RfidReader
import signal
import time
import RPi.GPIO as GPIO
import spi

from libraries.nfc_522 import Nfc522
from util.constants import FridgeHardware, Server, FridgeStatus
from threading import Timer
from libraries.server_request import ServerRequest

leitorMFRC522 = None
continue_reading = True
openingTimer = None
closingTimer = None


###########################################################################################################################

def handle_finish_software(signal, frame):
    """
    Manipula o evento de fechamentodo programa
    :param signal:
    :param frame:
    :return:
    """
    global continue_reading
    continue_reading = False
    try:
        spi.closeSPI()
    except Exception as e:
        print(e)

    GPIO.cleanup()


###########################################################################################################################

def cancel_timers():
    """
    Cancela todos os timers em andamento
    """
    global openingTimer, closingTimer

    if closingTimer != None and closingTimer.is_alive():
        closingTimer.cancel()
        closingTimer = None

    if openingTimer != None and openingTimer.is_alive():
        openingTimer.cancel()
        openingTimer = None


###########################################################################################################################

def timeout_opening_fridge():
    print("Timeout_opening")

    # Debouncing do detecção de porta fechada.
    if GPIO.input(FridgeHardware.FRIDGE_DOOR_BUTTON_PIN):
        srv_request = ServerRequest(Server.URL_SERVER, Server.DEFAULT_CONTEXT)  # Enviar requisição para o servidor informando porta aberta
        srv_request.open_door_alert()


###########################################################################################################################


# Mandar requisição pro servidor indicando porta aberta
def timeout_closing_fridge():
    global leitorMFRC522

    print("Timeout_closing")

    # Fazer leituras dos RFID e mandar pro server
    data = leitorMFRC522.get_rfid_mac_list()  # Engatilha a leitura de RFID
    print("Leitura:\t" + str(data))

    # Verificação de porta ainda fechada.
    if GPIO.input(FridgeHardware.FRIDGE_DOOR_BUTTON_PIN) == GPIO.LOW:
        srvRequest = ServerRequest(Server.URL_SERVER, Server.DEFAULT_CONTEXT)
        srvRequest.send_product_data(data_array=data)


###########################################################################################################################


def handle_fridge_on_open():
    """
    Função que lida com o evento de porta aberta
    :return: None
    """
    global openingTimer, closingTimer
    print("on open")

    cancel_timers()  # Cancela possíveis timers em andamento

    openingTimer = Timer(FridgeHardware.PERIOD_CLOSED_DOOR_ALERT_SEG, timeout_opening_fridge)
    openingTimer.start()


###########################################################################################################################
def handle_fridge_on_close():
    """
    Função que lida com o evento de porta fechada
    :return: None
    """
    print("on close")

    global openingTimer, closingTimer

    cancel_timers()  # Cancela possíveis timers em andamento

    closingTimer = Timer(FridgeHardware.PERIOD_CLOSED_DOOR_ALERT_SEG, timeout_closing_fridge)
    closingTimer.start()


def handle_fridge_door(channel):
    if GPIO.input(FridgeHardware.FRIDGE_DOOR_BUTTON_PIN):  # Caso 1 (rising)
        handle_fridge_on_open()

    else:  # Caso 0 (falling)
        handle_fridge_on_close()


###########################################################################################################################


def initial_setup():
    """
    Configurações realizadas antes da inicialização do software.
    """

    # Configurar servidor
    global leitorMFRC522
    GPIO.setmode(GPIO.BCM)

    leitorMFRC522 = Nfc522()
    print("Starting system")

    # Configurar botao como entrada com pull_up
    GPIO.setup(FridgeHardware.FRIDGE_DOOR_BUTTON_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    # Configurar evento de borda de subida e descida
    GPIO.add_event_detect(FridgeHardware.FRIDGE_DOOR_BUTTON_PIN, GPIO.BOTH, handle_fridge_door)  # Ao abrir/fechar a porta

    signal.signal(signal.SIGINT, handle_finish_software)  # Sinal de finalização do software (Ctrl + C)

    handle_fridge_door(None)  # Verificação inicial do status da porta
    print("System started")


###########################################################################################################################

if __name__ == '__main__':
    """
    Função principal
    """

    initial_setup()  # Configurações iniciais
    while continue_reading:  # Em loop até que o sinal de finalização seja executado
        time.sleep(5)
