#!/usr/bin/env python
# -*- coding: utf-8 -*-

import signal

from libraries.MFRC522 import MFRC522
from libraries.pinos import PinoControle
from util.util import MACUtil, EPCUtil


class Nfc522(object):
    pc = PinoControle()
    MIFAREReader = None
    RST1 = 22  # GPIO
    RST2 = 27  # GPIO
    RST_LIST = [22, 27]
    SPI_DEV0 = '/dev/spidev0.0'
    SPI_DEV1 = '/dev/spidev0.1'
    SPI_DEV_LIST = ['/dev/spidev0.0', '/dev/spidev0.1']

    def get_rfid_mac_list(self, autenticacao=False):

        """
        Faz a varredura em todos os leitores

        :param autenticacao: Se será feita a autenticação
        :return: Vetor contendo o MAC de cada
        """
        mac_list = []

        for i in range(min(len(self.RST_LIST), len(self.SPI_DEV_LIST)) - 1, -1, -1):
            _uid = []
            try:
                # for i in range(0, min(len(self.RST_LIST), len(self.SPI_DEV_LIST))):

                self.MIFAREReader = MFRC522(self.RST_LIST[i], self.SPI_DEV_LIST[i], 100000)

                (status, TagType) = self.MIFAREReader.MFRC522_Request(self.MIFAREReader.PICC_REQIDL)

                (status, uid) = self.MIFAREReader.MFRC522_Anticoll()

                _uid = uid
                # if status == self.MIFAREReader.MI_OK:
                #     mac_list.append(self.obtem_tag(self.MIFAREReader, status, uid, autenticacao))
                #     str_cont = self.obtem_tag(self.MIFAREReader, status, uid, autenticacao)
                #
                #     print("Conteudo tag: ")
                #     print(str_cont)
                # key = [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]
                # Select the scanned tag
                print("Status: ")
                print(status)
                if status == self.MIFAREReader.MI_OK:           # len(uid) > 0:
                    self.MIFAREReader.MFRC522_SelectTag(uid)
                    # Authenticate
                    # status = self.MIFAREReader.MFRC522_Auth(self.MIFAREReader.PICC_AUTHENT1A, 8, key, uid)

                    print("Reading tags' content")
                    # Check if authenticated
                    # if status == self.MIFAREReader.MI_OK:
                    # self.MIFAREReader.MFRC522_Read(5)

                    tmp_list = []

                    for k in range(0, 15): # 15 primeiros blocos
                        x = self.MIFAREReader.MFRC522_Read(k)
                        self.MIFAREReader.MFRC522_StopCrypto1()
                        for l in range(0, 4): # quatro primeiros bytes
                            tmp_list.append(x[l])

                    epc = EPCUtil.convert_array_epc(tmp_list) # Converte o array de bytes para o EPC code
                    mac_list.append(epc)
                else:
                    # self.MIFAREReader.MFRC522_SelectTag(_uid)
                    # x = self.MIFAREReader.MFRC522_Read(0)
                    # self.MIFAREReader.MFRC522_StopCrypto1()
                    self.pc.atualiza(self.RST_LIST[i], self.pc.baixo())
            except Exception as e:
                print("Exc Leitor " + str(i + 1) + ":\t" + str(e))
            finally:
                self.MIFAREReader.fecha_spi()
                del self.MIFAREReader

        return mac_list

    def obtem_nfc_rfid(self, autenticacao=False):
        try:
            self.MIFAREReader = MFRC522(self.RST1, self.SPI_DEV0)
            while True:
                (status, TagType) = self.MIFAREReader.MFRC522_Request(self.MIFAREReader.PICC_REQIDL)
                (status, uid) = self.MIFAREReader.MFRC522_Anticoll()

                if status == self.MIFAREReader.MI_OK:
                    return self.obtem_tag(self.MIFAREReader, status, uid, autenticacao)
                else:
                    self.pc.atualiza(self.RST1, self.pc.baixo())
                    self.MIFAREReader = MFRC522(self.RST2, self.SPI_DEV1)
                    while True:
                        (status, TagType) = self.MIFAREReader.MFRC522_Request(self.MIFAREReader.PICC_REQIDL)
                        (status, uid) = self.MIFAREReader.MFRC522_Anticoll()

                        if status == self.MIFAREReader.MI_OK:
                            return self.obtem_tag(self.MIFAREReader, status, uid, autenticacao)
                        else:
                            self.pc.atualiza(self.RST2, self.pc.baixo())
                            return None
        except Exception as e:
            print(e)
        finally:
            self.MIFAREReader.fecha_spi()

    def obtem_tag(self, MIFAREReader, status, uid, autenticacao):
        try:
            if autenticacao:
                # Chave padrão para a autenticação
                key = [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]
                MIFAREReader.MFRC522_SelectTag(uid)
                status = MIFAREReader.MFRC522_Auth(MIFAREReader.PICC_AUTHENT1A, 8, key, uid)
                if status == MIFAREReader.MI_OK:
                    MIFAREReader.MFRC522_Read(8)
                    MIFAREReader.MFRC522_StopCrypto1()
                else:
                    print("Erro na autenticação!")

                    return None
            tag_hexa = ''.join([str(hex(x)[2:4]).zfill(2) for x in uid[:-1][::-1]])  # Returns in hexadecimal
            return int(tag_hexa.upper(), 16)  # Returns in decimal
        except Exception as e:
            print(e)
