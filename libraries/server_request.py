import requests
import json

from util.constants import ErrorCode
from util.constants import FridgeStatus
from util.constants import Server
from util.constants import HttpStatusCode


class ServerRequest:
    """
    Server Requests
    """

    def __init__(self, url_server, context_path):
        self.url_server = url_server  # Url do server (http://ip:port)
        self.context_path = context_path  # Nome do projeto (tcc_server)

    def __get_request(self, resource_path, data):
        _url = self.url_server + self.context_path + resource_path  # Montagem da url
        r = requests.get(url=_url, params=data)  # Executa a consulta GET
        print(r.status_code)

        if r.status_code == HttpStatusCode.HTTP_STATUS_OK:  # Caso a requisição seja feita com sucesso
            print(r.text)
            return r.text

        else:  # Caso algum erro aconteça
            return str(ErrorCode.STATUS_GET_REQUEST_ERROR)

    def __post_request(self, resource_path, data):

        _url = self.url_server + self.context_path + resource_path              # Montagem da url
        print("URL:\t" + _url)

        _content = str(data)

        r = requests.post(url=_url, data=_content)                               # Executa a consulta POST
        print("Status code:\t" + str(r.status_code))

        if r.status_code == HttpStatusCode.HTTP_STATUS_OK:                      # Caso a requisição seja feita com sucesso
            print("Data from request:\t" + str(r.text))

            return r.text
        else:  # Caso algum erro aconteça
            return str(ErrorCode.STATUS_POST_REQUEST_ERROR)

    def send_product_data(self, data_array):

        # descobrir como fazer append em json
        mac_str = "{\"id_fridge\" : 1, \"epc_codes\":[ "

        str_list = ""

        for i in range(0, len(data_array)):
            if i > 0:
                str_list = str_list + ", "
            str_list = str_list +  "\""  + str(data_array[i]) + "\""

        mac_str = mac_str + str_list + " ]}"

        print("JSON tags:\t" + mac_str)
        # js = json.loads(mac_str)  # Armazena a string num objeto JSON para verificar se está certo

        # Adição do parâmetro de tipo de URL
        # EX: /register?type_registry=1
        _resource = Server.INTERACTION_REGISTER_WS_URL + "?" + Server.PARAM_TYPE_REG + "=" + Server.REGISTER_URL_CODE

        # Finalizar com a requisição de POST de acordo com a implementação do servidor
        self.__post_request(resource_path=_resource, data=mac_str)

    def open_door_alert(self):

        _js = "{\"id_fridge\" : 1, \"record_type\": \"open_door\", \"door_status\" : " + str(FridgeStatus.FRIDGE_DOOR_OPENED) + "}"  # Criação da estrutura em JSON
        print("JSON:\t" + _js)
        _json_obj = json.loads(_js)  # Criação do objeto JSON

        _resource = Server.INTERACTION_REGISTER_WS_URL + "?" + Server.PARAM_TYPE_REG + "=" + Server.OPEN_DOOR_URL_CODE   # Adição do parâmetro de
        # tipo de URL

        self.__post_request(_resource, str(_json_obj))  # Requisição post
        
        
        
