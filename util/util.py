import time


class MACUtil:
    @staticmethod
    def convert_array_to_mac(array_mac):

        if len(array_mac) == 0:
            return ""

        mac_str = ""

        for hex in array_mac:
            mac_str = mac_str + "%0.2X:" % hex

        if len(mac_str) > 0:
            mac_str = mac_str[0:-1]

        return mac_str


class EPCUtil:
    @staticmethod
    def convert_array_epc(array_mac):
        init_pos = 4 * 5 + 3

        #        print(array_mac)

        tmp_list = []
        for i in range(init_pos, len(array_mac)):

            if array_mac[i] == 0xFE:
                j = 0
                has_non_zero = False

                for j in range(i, len(array_mac)):

                    if array_mac[i] != 0x00:
                        has_non_zero = True
                        break

                if has_non_zero:
                    break

            tmp_list.append(array_mac[i])

        print("\n\n" + str(tmp_list))

        epc_code = 0

        if len(tmp_list) > 0:
            for i in range(len(tmp_list)):
                bit_op = (len(tmp_list) - 1) - i

                epc_code = epc_code + (tmp_list[i] << bit_op * 8)

        print("EPC code: " + str(epc_code))

        return str(epc_code)
