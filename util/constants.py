"""

"""


class HttpStatusCode:
    HTTP_STATUS_OK = 200


class FridgeHardware:
    FRIDGE_DOOR_BUTTON_PIN = 4
    PERIOD_OPENED_DOOR_ALERT_SEG = 5
    PERIOD_CLOSED_DOOR_ALERT_SEG = 2


class FridgeStatus:
    FRIDGE_DOOR_OPENED = 0
    FRIDGE_DOOR_CLOSED = 1


class Server:
    IP_SERVER = "192.168.0.105"     # "192.168.1.201"  # "10.0.0.106"
    PORT_SERVER = "8080"
    URL_SERVER = "http://" + IP_SERVER + ":" + PORT_SERVER
    DEFAULT_CONTEXT = "/tcc_server"
    INTERACTION_REGISTER_WS_URL = "/register"
    PARAM_TYPE_REG = "type_record"
    PARAM_DATA = "data"

    OPEN_DOOR_URL_CODE = "2"
    REGISTER_URL_CODE = "1"


class ErrorCode:
    STATUS_OK = 0
    STATUS_GET_REQUEST_ERROR = -1
    STATUS_POST_REQUEST_ERROR = -2
